public class Queue {

    private int front;
    private int rear;
    private int capacity;
    private int count;
    private int[] queue;


    public Queue(int capacity) {
        this(capacity, false);
    }

    public Queue(int capacity, boolean dynamic) {
        queue = new int[capacity];
        this.capacity = capacity;
        front = 0;
        rear = -1;
        count = 0;
    }

    public void enqueue(int data) {
        if (isFull()) {
            throw new IllegalStateException("full queue");
        }
        rear = (rear + 1) % capacity;
        queue[rear] = data;
        count++;
    }

    public int dequeue() {
        if (isEmpty()) {
            throw new IllegalStateException("empty queue");
        }
        int item = this.queue[this.front];
        this.front = (this.front + 1) % this.capacity;
        this.count--;
        return item;
    }

    public boolean isFull() {
        return (size() == capacity);
    }

    public boolean isEmpty() {
        return (size() == 0);

    }

    public int size() {//return the number of elements in queue
        return count;
    }

    public int peek() {//return element at top(front) of the queue
        if (isEmpty()) {
            throw new IllegalStateException("empty queue");
        }
        return queue[front];
    }

    public boolean isDynamic() {
        return false;
    }
}
