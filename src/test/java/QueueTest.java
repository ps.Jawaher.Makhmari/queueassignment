import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class QueueTest {

    @Test
    public void givenFixedQueue_whenEnqueueAndDequence_thenWorkAsExpected() throws IllegalAccessException {
        Queue queue = new Queue(5);
        Assertions.assertEquals(queue.size(), 0);

        IllegalStateException thrown =
                Assertions.assertThrows(IllegalStateException.class,
                        () -> queue.dequeue());
        Assertions.assertEquals("empty queue", thrown.getMessage());


        thrown = Assertions.assertThrows(IllegalStateException.class, () -> queue.peek());
        Assertions.assertEquals("empty queue", thrown.getMessage());

        queue.enqueue(10);
        queue.enqueue(20);

        Assertions.assertEquals(2, queue.size());
        Assertions.assertEquals(10, queue.peek());
        Assertions.assertEquals(10, queue.dequeue());

        queue.enqueue(30);
        queue.enqueue(40);
        Assertions.assertEquals(3, queue.size());
        Assertions.assertEquals(20, queue.peek());

        queue.enqueue(50);
        queue.enqueue(60);
        Assertions.assertEquals(5, queue.size());
        thrown = Assertions.assertThrows(IllegalStateException.class, () -> queue.enqueue(70));
        Assertions.assertEquals("full queue", thrown.getMessage());

        Assertions.assertEquals(20, queue.dequeue());
        Assertions.assertEquals(30, queue.dequeue());
        Assertions.assertEquals(40, queue.dequeue());
        Assertions.assertEquals(50, queue.dequeue());
        Assertions.assertEquals(60, queue.dequeue());
        Assertions.assertEquals(0, queue.size());

        thrown = Assertions.assertThrows(IllegalStateException.class, () -> queue.dequeue());
        Assertions.assertEquals("empty queue", thrown.getMessage());

    }

    @Test
    public void givenDynamicQueue_whenEnqueueAndDequence_thenWorkAsExpected() throws IllegalAccessException {
        Queue queue = new Queue(5, true);
        Assertions.assertTrue(queue.isDynamic());
        Assertions.assertEquals(queue.size(), 0);

        IllegalStateException thrown =
                Assertions.assertThrows(IllegalStateException.class,
                        () -> queue.dequeue());
        Assertions.assertEquals("empty queue", thrown.getMessage());


        thrown = Assertions.assertThrows(IllegalStateException.class, () -> queue.peek());
        Assertions.assertEquals("empty queue", thrown.getMessage());

        queue.enqueue(10);
        queue.enqueue(20);

        Assertions.assertEquals(2, queue.size());
        Assertions.assertEquals(10, queue.peek());
        Assertions.assertEquals(10, queue.dequeue());

        queue.enqueue(30);
        queue.enqueue(40);
        Assertions.assertEquals(3, queue.size());
        Assertions.assertEquals(20, queue.peek());

        queue.enqueue(50);
        queue.enqueue(60);
        Assertions.assertEquals(5, queue.size());
        queue.enqueue(70);
        queue.enqueue(80);
        Assertions.assertEquals(7, queue.size());

        Assertions.assertEquals(20, queue.dequeue());
        Assertions.assertEquals(30, queue.dequeue());
        Assertions.assertEquals(40, queue.dequeue());
        Assertions.assertEquals(50, queue.dequeue());
        Assertions.assertEquals(60, queue.dequeue());

        Assertions.assertEquals(2, queue.size());

        Assertions.assertEquals(70, queue.dequeue());
        Assertions.assertEquals(80, queue.dequeue());

        Assertions.assertEquals(0, queue.size());

        thrown = Assertions.assertThrows(IllegalStateException.class, () -> queue.dequeue());
        Assertions.assertEquals("empty queue", thrown.getMessage());

    }

}
